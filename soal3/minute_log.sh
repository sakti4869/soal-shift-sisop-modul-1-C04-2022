
#!/bin/bash#

dir="/home/$USER/log"
log=$dir/$(date +"metrics_%Y%m%d%H%M%S").log

sudo mkdir -p $dir
chmod 700 $dir

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" | sudo tee -a "$log"
free -m "/home/$USER" | awk 'BEGIN{ORS=","} NR>1{for(i=2;i<=NF;i++)print $i}' | sudo tee -a "$log"
printf "/home/$USER", | sudo tee -a "$log"
du -sh "/home/$USER" | awk '{print $1}' | sudo tee -a "$log"

chmod 400 $log
