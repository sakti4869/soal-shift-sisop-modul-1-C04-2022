 
#!/bin/bash#

#### Pengatar Registrasi ####
echo "WELCOME, PLEASE REGISTER!"

#### User memasukkan username yang diinginkan ####
echo "Username :"
read username

function checkPass(){
	if [ "${#password}" -ge 8 ]; then										#### Untuk mengecek panjang password ####
		if [[ $(echo "$password" | awk '/[a-z]/ && /[A-Z]/') ]]; then		#### Untuk mengecek apakah password memiliki huruf kapital dan kecil ####
			if [[ "$password" =~ ['!@#$%^&*()_+'] ]]; then					#### Untuk mengecek apakah password sudah alphanumeric ####
				echo "Password must be an alphanumeric string!"
			else
				if [ "$password" == "$username" ]; then						#### Untuk mengecek apakah password sama dengan username ####
					echo "Password must not the same as the username!"
				else
					echo "Username : ${username}" >> users/user.txt
					echo "Password : ${password}" >> users/user.txt
				fi
			fi
		else
			echo "Password must contain upper and lower case!"
		fi
	else
		echo "Password must contain minimum 8 characters!"
	fi
}

time=`date +'%D %T'`

	#### Untuk mengecek apakah username sudah terdaftar atau belum ####
	if grep -qF "$username" users/user.txt; then						
		status="$time REGISTER: ERROR User already exist"
		echo "${status}" >> log.txt
	else
		echo "Password :"													#### Jika belum maka user diizinkan untuk memasukkan password ####
		read -s password
		checkPass
		status="$time REGISTER: INFO User $username registered succesfully"
		echo "${status}" >> log.txt
	fi 

