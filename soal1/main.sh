
#!/bin/bash#

#### Pengantar Login ####
echo "CONTINUE LOGIN"

#### User memasukkan username yang diinginkan ####
echo "Username :"
read username

time=`date +'%D %T'`

if grep -qF "$username" users/user.txt; then				#### Mengecek apakah username yang diinginkan sudah terdaftar ####
	echo "Password :"										#### Jika sudah, maka user akan diminta untuk memasukkan password ####
	read -s password
	
	if grep -qF "$password" users/user.txt; then
		status="$time LOGIN: INFO $username logged in"	#### Jika password benar maka data login akan tersimpan di file log.txt ####
		echo "$status" >> log.txt
		echo "1) Download Images ('dl N')"
		echo "2) Login Attempts ('att')"
		read command
		number=$(echo "$command" | tr -dc '0-9')
		
		if [[ "$command" == *"dl"* ]]; then
			date=$(date +'%Y-%m-%d')
			folder="${date}_${username}"

			if [[ -f "${folder}.zip" ]]; then
				unzip "${folder}"
				rm "${folder}.zip"
				cd "${folder}"
				nu=$(ls | wc -l | tr -dc '0-9')
				let numFile=nu+1
				let file=number+nu
				
				for ((no=numFile; no<=file; no=no+1))
				do
					wget -O "PIC_0$no.jpg" https://loremflickr.com/cache/resized/65535_51785638899_1cb948f890_n_320_240_nofilter.jpg
				done
				cd ..
				zip -P "$password" -r "${folder}" "${folder}"
				rm -r "${folder}"
			else
				mkdir "${folder}"
				cd "${folder}"
				for ((nu=1; nu<=number; nu=nu+1))
				do
					wget -O "PIC_0$nu.jpg" https://loremflickr.com/cache/resized/65535_51785638899_1cb948f890_n_320_240_nofilter.jpg
				done
				cd ..
				zip -P "$password" -r "${folder}" "${folder}"
				rm -r "${folder}"
			fi
		else
			success=0
			failed=0
			attempt="log.txt"
			while IFS= read -r login
			do
				if [[ $login == *${username}*"logged in" ]]; then 
					let success+=1
				elif [[ $login == *"on user"*${username} ]]; then 
					let failed+=1
				fi
			done < "$attempt"
			
			echo "Success  : ${success}"
			echo "Failed   : ${failed}"
		fi
	else
		status="$time LOGIN: ERROR failed login attempt on user $username"
		echo "$status" >> log.txt
	fi
else
	echo "PLEASE REGISTER FIRST!"
	bash register.sh
fi
