
#!/bin/bash

folder=forensic_log_website_daffainfo_log
file="log_website_daffainfo.log"

### A.Membuat folder forensic_log_website_daffainfo_log ####
mkdir -p $folder

### B.Rata rata request per jam ###
cat $file | awk ' END {print "Rata-rata serangan adalah sebanyak", (NR-1)/12, "requests per jam"}'>$folder/ratarata.txt

### C. IP paling banyak melakukan request ###
cat $file | awk -F\" '{print $2}' $file | sort | uniq -c | sort -rn | head -n1 | awk '{print "IP yang paling banyak mengakses server adalah:", $2, "sebanyak", $1, "requests"} '>$folder/result.txt

### D. Banyak request yang menggunakan user-agent curl ###
printf "\n\n">>result.txt
cat $file | awk '/curl/ { n++ } END{ print "Ada", n ,"request yang menggunakan curl sebagai user-agent" }'>>$folder/result.txt

### E. Daftar IP yang mengakses pada jam 2 pagi ###
cat $file | awk -F':|"' '{if($6 == 2)print $2}' $file | sort | uniq >> $folder/result.txt
