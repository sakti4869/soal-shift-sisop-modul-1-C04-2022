# Soal-Shift-Sisop-Modul-1-C04-2022 

### Anggota Kelompok
* Meisya Salsabila Indrijo Putri  5025201114
* Muhammad Haikal Aria Sakti 05111940000088
* Marcelino Salim 5025201026

# Soal 1

### register.sh
Script [register.sh](https://gitlab.com/sakti4869/soal-shift-sisop-modul-1-C04-2022/-/blob/main/Soal%201/register.sh) digunakan untuk register user. Di awal program terdapat welcome message dan user diminta untuk memasukkan username seperti pada script di bawah. Nanti data yang di input akan tersimpan ke dalam `users/user.txt`.

	echo "WELCOME, PLEASE REGISTER!"

	echo "Username :"
	read username

Kemudian username yang sudah diinput akan di cek terlebih dahulu, apakah data username tersebut sudah sudah ada pada `users/user.txt`.

	if grep -qF "$username" users/user.txt; then						
		status="$time REGISTER: ERROR User already exist"
		echo "${status}" >> log.txt

* Jika username yang diinputkan sudah terdaftar, maka akan ditampilkan pesan error. Pesan error juga akan dimasukkan ke dalam file `log.txt` beserta waktu percobaan register.
* Fungsi untuk waktu percobaan register, menggunakan `time= date +'%D %T'`

Apabila username belum terdaftar, maka program akan melanjutkan dengan meminta user untuk menginputkan password.

	echo "Password :"													
	read -s password

Kemudian program akan mengecek apakah password yang diinputkan oleh user sudah sesuai dengan syarat yang ada dan akan langsung tersimpan ke dalam `users/user.txt`. Tetapi jika belum sesuai, maka akan ditampilkan pesan syarat apa yang belum terpenuhi. 

	function checkPass(){
		if [ "${#password}" -ge 8 ]; then										
			if [[ $(echo "$password" | awk '/[a-z]/ && /[A-Z]/') ]]; then		
				if [[ "$password" =~ ['!@#$%^&*()_+'] ]]; then					
					echo "Password must be an alphanumeric string!"
				else
					if [ "$password" == "$username" ]; then						
						echo "Password must not the same as the username!"
					else
						echo "Username : ${username}" >> users/user.txt
						echo "Password : ${password}" >> users/user.txt
					fi
				fi
			else
				echo "Password must contain upper and lower case!"
			fi
		else
			echo "Password must contain minimum 8 characters!"
		fi
	}

* Fungsi `checkPass` digunakan untuk mengecek apakah password sesuai dengan syarat.
* `"${#password}" -ge 8` panjang password minimal 8 karakter.
* `$(echo "$password" | awk '/[a-z]/ && /[A-Z]/')` password harus terdapat minimal satu huruf UPPER dan LOWER.
* `"$password" =~ ['!@#$%^&*()_+'` password harus berupa alphanumeric.
* `"$password" == "$username"` password tidak boleh sama dengan username.

Data yang sudah tersimpan menandakan bahwa proses register user telah berhasil. Lalu pesan log akan dimasukkan ke dalam file `log.txt` beserta waktu saat register telah berhasil.

	status="$time REGISTER: INFO User $username registered succesfully"
	echo "${status}" >> log.txt


### main.sh
Script bash [main.sh](https://gitlab.com/sakti4869/soal-shift-sisop-modul-1-C04-2022/-/blob/main/soal1/main.sh) digunakan untuk login user. Pada 'main.sh user dapat mendownload beberapa gambar dan menampilkan jumlah percobaan login yang dilakukan user baik berhasil maupun gagal. Pada awal program akan muncul welcome message dan user diminta untuk menginput username.

	echo "CONTINUE LOGIN"

	echo "Username :"
	read username

Program akan mengecek apakah username yang diinputkan sudah terdaftar pada `users/user.txt` atau belum. Jika belum maka program akan menampilkan message dan bash script `register.sh`, sebaliknya jika sudah maka program akan meminta user untuk menginputkan password.

	if grep -qF "$username" users/user.txt; then				
		echo "Password :"										
		read -s password
	else
		echo "PLEASE REGISTER FIRST!"
		bash register.sh
	fi

Kemudian program akan mengecek apakah password yang diinputkan sudah sesuai dengan yang tersimpan.Dan program akan menampilkan message pada file `log.txt` yang berisikan status apakah proses login berhasil atau gagal.

	if grep -qF "$password" users/user.txt; then
		status="$time LOGIN: INFO $username logged in"	
		echo "$status" >> log.txt
	else
		status="$time LOGIN: ERROR failed login attempt on user $username"
		echo "$status" >> log.txt
	fi

Ketika proses login berhasil, user dipersilahkan untuk menginputkan perintah yang diinginkan.

	echo "1) Download Images ('dl N')"
	echo "2) Login Attempts ('att')"
	read command
	number=$(echo "$command" | tr -dc '0-9')

Jika user memasukkan command `dl N` maka program akan mendownload gambar sesuai dengan jumlah `N` yang diinginkan user dan akan dijadikan 1 kedalam file `YYYY-MM-DD_USERNAME.zip`.

	if [[ "$command" == *"dl"* ]]; then
		date=$(date +'%Y-%m-%d')
		folder="${date}_${username}"

		if [[ -f "${folder}.zip" ]]; then
			unzip "${folder}"
			rm "${folder}.zip"
			cd "${folder}"
			nu=$(ls | wc -l | tr -dc '0-9')
			let numFile=nu+1
			let file=number+nu
			
			for ((no=numFile; no<=file; no=no+1))
			do
				wget -O "PIC_0$no.jpg" https://loremflickr.com/cache/resized/65535_51785638899_1cb948f890_n_320_240_nofilter.jpg
			done
			cd ..
			zip -P "$password" -r "${folder}" "${folder}"
			rm -r "${folder}"
		else
			mkdir "${folder}"
			cd "${folder}"
			for ((nu=1; nu<=number; nu=nu+1))
			do
				wget -O "PIC_0$nu.jpg" https://loremflickr.com/cache/resized/65535_51785638899_1cb948f890_n_320_240_nofilter.jpg
			done
			cd ..
			zip -P "$password" -r "${folder}" "${folder}"
			rm -r "${folder}"
		fi

* Variabel `number` untuk menyimpan nilai `N` yaitu banyak gambar yang ingin didowload oleh user.
* Variabel `date` untuk menyimpan tanggal yang akan digunakan untuk penamaan file.
* Variabel `folder` untuk menyimpan nama folder.
* Jika folder yang dituju sudah ada, maka program akan menghapusnya terlebih dahulu dan kemudian akan dibuat folder baru dengan nama seperti sebelumnya.
* Download gambar menggunakan `wget -O "PIC_0$nu.jpg" https://loremflickr.com/cache/resized/65535_51785638899_1cb948f890_n_320_240_nofilter.jpg`. 
* Setelah selesai mendownload gambar sesuai jumlah yang diinginkan, maka folder dizip kembali menggubakan password `zip -P "$password" -r "${folder}" "${folder}"`.

Jika user memasukkan command `att`, maka program akan mengecek data login pada file `log.txt`. 

	success=0
	failed=0
	attempt="log.txt"
	while IFS= read -r login
	do
		if [[ $login == *${username}*"logged in" ]]; then 
			let success+=1
		elif [[ $login == *"on user"*${username} ]]; then 
			let failed+=1
		fi
	done < "$attempt"
			
	echo "Success  : ${success}"
	echo "Failed   : ${failed}"

### Kendala dan Dokumentasi:
* Masih bingung memahami fungsi `grep -qF`
* Bingung mencari cara untuk zip dan unzip file

![Screenshot__93_](/uploads/7c839ec0c2ff87106c3109c586016045/Screenshot__93_.png)
![Screenshot__3_](/uploads/69108e7c1f8b4b9d1a4b0f76b66d96c8/Screenshot__3_.jpeg)

# Soal 2

### 2.A
Membuat folder terlebih dahulu bernama forensic_log_website_daffainfo_log.

	folder=forensic_log_website_daffainfo_log
	mkdir -p $folder

### 2.B
Menghitung rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama `ratarata.txt`. Dengan menggunakan fungsi awk seperti berikut;

	file="log_website_daffainfo.log"
	cat $file | awk ' END {print "Rata-rata serangan adalah sebanyak", (NR-1)/12, "requests per jam"}'>$folder/ratarata.txt

### 2.C
Menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Hailnya akan masuk kedalam file baru bernama `result.txt`. Dengan menggunakan fungsi awk;

	cat $file | awk -F\" '{print $2}' $file | sort | uniq -c | sort -rn | head -n1 | awk '{print "IP yang paling banyak mengakses server adalah:", $2, "sebanyak", $1, "requests"} '>$folder/result.txt

### 2.D
Menghitung berapa banyak requests yang menggunakan user-agent curl. Kemudian masukkan berapa banyak requestnya kedalam file bernama `result.txt` yang telah dibuat sebelumnya.

	printf "\n\n">>result.txt
	cat $file | awk '/curl/ { n++ } END{ print "Ada", n ,"request yang menggunakan curl sebagai user-agent" }'>>$folder/result.txt

### 2.E
Diminta untuk menampilkan semua request yang dilakukan pada jam 2 pagi.

	cat $file | awk -F':|"' '{if($6 == 2)print $2}' $file | sort | uniq >> $folder/result.txt

### Kendala & Dokumentasi:
* Masih kesulitan dalam menggunakan fungsi `awk`
* Folder yang terbuat sempat tidak dapat terakses

![Screenshot__4_](/uploads/f52164ca0527e815c667c7122ab7ceaf/Screenshot__4_.jpeg)

# Soal 3

### 3.A & B

	dir="/home/$USER/log"
	log=$dir/$(date +"metrics_%Y%m%d%H%M%S").log

	sudo mkdir -p $dir
	chmod 700 $dir

	echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" | sudo tee -a "$log"
	free -m "/home/$USER" | awk 'BEGIN{ORS=","} NR>1{for(i=2;i<=NF;i++)print $i}' | sudo tee -a "$log"
	printf "/home/$USER", | sudo tee -a "$log"
	du -sh "/home/$USER" | awk '{print $1}' | sudo tee -a "$log"

Program untuk memonitoring ram dan size directory user. Adapun langkah yang dapat dilakukan yaitu :
* Membuat file `.log` untuk menyimpan data ram dan size directory dengan format `log=$dir/$(date +"metrics_%Y%m%d%H%M%S").log`.
* Memonitor ram menggunakan `free -m` dan mengurutkan data menggunakan fungsi awk , untuk mengakses size directory menggunakan `du -sh`.
* Keluaran dari data tersebut akan disimpan pada folder yang telah ditentukan.
* Lalu fungsi tersebut diharapkan dapat berjalan secara otomatis pada setiap menit  menggunakan  `crontab -e`    `* * * * * bash minute_log.sh`.

### 2.C
Script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis.
	
	date=$(date '+%Y%m%d%H')
	dir="/home/$USER/log/"
	target="$(find /home/"$USER"/log/*metrics_"$(date '+%Y%m%d%H')"*)"
	log="$dir""metrics_agg_""$date"".log"

	sudo mkdir -p $dir
	echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" | sudo tee -a "$log"
	printf "minimum," | sudo tee -a "$log"  && { readarray -t fs <<< $target; for i in "${fs[@]}"; do sed -n "2p" "$i"; done; } | cut -d"," -f1-10 | sort -n | head -1 | sudo tee -a "$log"
	printf "maksimum," | sudo tee -a "$log"  && { readarray -t fs <<< $target; for i in "${fs[@]}"; do sed -n "2p" "$i"; done; } | cut -d"," -f1-10 | sort -n | tail -1 | sudo tee -a "$log" 
	{ readarray -t fs <<< $target ;for i in "${fs[@]}" ; do sed -n 2p "$i"; done; } | awk -F'[,]' '{ for (i=1;i<=NF;i++){if(i==9) {sum[i]=$i} else {sum[i]+=$i}} } END { printf "average,"; for (i=1;i<=NF;i++){if(i==9) {printf sum[i]} else {printf sum[i]/NR}; if (i!=NF) {printf ","} else {printf "\n"}}}' | sudo tee -a "$log"

### Dokumentasi:
![aggregate](/uploads/e3e601e39e1aa220b0f7c517442f4692/aggregate.jpeg)

![log](/uploads/3b55c027ab1167c7871a2a9dbb4686c3/log.jpeg)

![Minute](/uploads/7b60fffe4b3a236d269777bd371ada20/Minute.jpeg)
